﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TreeTamerClient.ServiceReference2;

namespace TreeTamerClient
{
    class Program
    {
        static void Main(string[] args)
        {

            ServiceReference2.ValidateClient proxy = new ServiceReference2.ValidateClient();
            string username;
            string password;

            Console.Out.WriteLine("Please enter your user name:  ");
            username = Console.ReadLine();
            Console.Out.WriteLine("Please enter your password:  ");
            password = Console.ReadLine();

            Console.Out.WriteLine("Authenticating user");
            bool s = proxy.AuthenticateUser(username, password);
            
            if (s == true)
                Console.Out.WriteLine("You are now authenicated");
            else
                Console.Out.WriteLine("Username and Password Invalid");

            Console.ReadLine();


        }
    }
}
